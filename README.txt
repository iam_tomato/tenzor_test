Проект состоит из следующих файлов:
    company_bd.py - исходный код рпограммы
    bd_conf.yml - файл концигурации БД
    complexity.txt - описание сложности алгоритма
    employees.json - json из которого можно загрузить БД
    requirements.txt - необходимые библиотеки

Программа была написана на python 3.6
Данная программа по указанному id сотудника из базы данных выводит всех сотрудников из того же города.
Запуск:
    python company_bd.py
Запуск с данными из my_db.json:
    python company_bd.py ./my_db.json
Данные для подключения к базе данных хранятся в db_conf.yml. Измените для доступа к вашей бд.
Программа читает данные из таблицы company. При запуске без каких-либо аргументов, программа считает, что данная таблица
уже имеется и работает с ней. Если нужно создать новую таблицу с данными из файла, укажите путь к JSON файлу как аргумент
в виде:
    python company_bd.py ./my_db.json
