import sys
import os
import json
import yaml
import psycopg2


def get_path(file_path):
    script_dir = os.path.dirname(__file__)
    abs_file_path = os.path.join(script_dir, file_path)
    return abs_file_path


class CompanyBD:
    _table_name = "company"
    
    def __init__(self, create_bd=False, bd_json_path='./employees.json'):
        self._conn = self._bd_connect()
        self._cursor = self._conn.cursor()
        
        if self._table_exists():
            if create_bd:
                self._drop_table()
                self._bd_from_json(bd_json_path)
        else:
            self._bd_from_json(bd_json_path)
    
    def _bd_connect(self):
        """
        Connects to postgres DB with configs from ./bd_conf.yml file
        """
        with open(get_path("bd_conf.yml"), 'r') as ymlfile:
            cfg = yaml.load(ymlfile)['postgres']
        
        try:
            return psycopg2.connect(
                f"dbname={cfg['dbname']} user={cfg['user']} host={cfg['host']} password={cfg['password']}")
        except psycopg2.DatabaseError as e:
            raise Exception(f"Unable to connect to database: {e}")
    
    def _table_exists(self):
        self._cursor.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'""")
        tables = [table[0] for table in self._cursor.fetchall()]
        if self._table_name in tables:
            return True
        else:
            return False
    
    def get_employee_by_id(self, emp_id):
        """
        Returns all employees from specified employee's city
        :param emp_id: employee id
        """
        if not self._is_leaf(emp_id):
            print("WRONG INPUT: Input id of employee.")
            exit()
        try:
            root_id = self._get_root(emp_id)
            leaf_ids = []
            self._get_leafs(root_id, leaf_ids)
        except IndexError:
            print("WRONG INPUT: Input id of employee.")
            exit()
        self._cursor.execute(f"SELECT name FROM company WHERE id IN {tuple(leaf_ids)}")
        return [item[0] for item in self._cursor]

    def _get_root(self, current_id):
        p_ids = []
    
        while current_id:
            self._cursor.execute(f"SELECT parent_id FROM company WHERE id={current_id}")
            current_id = self._cursor.fetchone()[0]
            p_ids.append(current_id)
        return p_ids[-2]

    def _get_leafs(self, current_id, leafs):
        self._cursor.execute(f"SELECT id FROM company WHERE parent_id={current_id}")
        ids = [item[0] for item in self._cursor]
        if ids:
            for c_id in ids:
                self._get_leafs(c_id, leafs)
        else:
            leafs.append(current_id)
        
    def _create_table(self):
        """
        Creates table "company"
        """
        self._drop_table()
        try:
            self._cursor.execute(f"""
                CREATE TABLE {self._table_name}(
                  id        SERIAL PRIMARY KEY,
                  parent_id INTEGER,
                  name      VARCHAR(255) NOT NULL,
                  type      INTEGER NOT NULL);""")
            self._conn.commit()
        except psycopg2.ProgrammingError as e:
            raise Exception(f"Unable to create the table: {e}")
    
    def _bd_from_json(self, json_path):
        """
        Loads data to the table "company" from json file
        :param json_path: path to json
        """
        
        self._create_table()
        
        insert_command_1 = """
            INSERT INTO company(id, parent_id, name, type) 
            VALUES (%s, %s, '%s', %s)
            """
        insert_command_2 = """
            INSERT INTO company(id, name, type) 
            VALUES (%s, '%s', %s)
            """
        
        with open(get_path(json_path), encoding='utf-8') as f:
            company_json = json.load(f)
        try:
            for record in company_json:
                if record['ParentId']:
                    self._cursor.execute(
                        insert_command_1 % (record['id'], record['ParentId'], record['Name'], record['Type']))
                else:
                    self._cursor.execute(insert_command_2 % (record['id'], record['Name'], record['Type']))
            self._conn.commit()
        except psycopg2.ProgrammingError as e:
            raise Exception(f"Unable to insert data to the table: {e}")
    
    def _drop_table(self):
        try:
            self._cursor.execute("""DROP TABLE company;""")
            self._conn.commit()
        except psycopg2.ProgrammingError:
            self._conn.rollback()

    def _is_leaf(self, current_id):
        self._cursor.execute(f"SELECT id FROM company WHERE parent_id={current_id}")
        child = self._cursor.fetchone()
        return not child

if len(sys.argv) > 1:
    bd_json = sys.argv[1]
    bd = CompanyBD(create_bd=True, bd_json_path=bd_json)
else:
    bd = CompanyBD()

employee_id = input('Enter employee id: ')
try:
    employee_id = int(employee_id)
except ValueError:
    print("WRONG INPUT: Employee id must be an integer.")
    exit()

employees = bd.get_employee_by_id(employee_id)
if employees:
    print(f"Employees from the same city with person whose id is {employee_id}:")
    for employee in employees:
        print(employee)
else:
    print(f'WRONG INPUT: An id {employee_id} is not a valid employee id')
